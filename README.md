# Bank App White List

To create a complete list of apps deployed as authorised "Mobile Banking" apps of banks in India.


# Resources
- [RBI Approved Mobile Banking enabled Banks](https://www.rbi.org.in/Scripts/bs_viewcontent.aspx?Id=2463)
- List of banks in NEFT and RTGS
- [Razorpay IFSC Dataset](https://github.com/razorpay/ifsc)
- [Google Play API](https://gplayapi.cashlessconsumer.in)


# Roadmap

- [ ] Banks - Google Play developer mapping
- [ ] List of all Apps from Bank developers.
- [ ] List of Vendors and Bank-Vendor mapping
- [ ] App Metadata of all banking apps.
- [ ] Confidence score for data / apps.
