# TODO

1. List of Bank Websites for 750 banks approved for mobile banking - RBI Scraper
2. List of developer Ids, their app count, email, site address on all apps.
3. List of Bank Websites for 1350 banks in India.

X. Delta Run mode.

# Thoughts

1. Expectation - Dev Ids for large install base banks won't change
2. Expectation - Small banks - M&A - vendor change - can all lead to app change.
3. Get list of un-updated apps
4. 


# Database design

Bank Master
IFSC PREFIX, Bank Name, Bank Website, Bank Developer

Play Developer
Play Developer, Bank/Vendor Flag, Entity Website, Email, Number of Apps

Vendor Data
Vendor Name, Banks serviced, Vendor Website

