import json
import time
import logging
import csv
import requests
import textdistance
import pandas as pd

def calculate_similarity(bank_name, app_name):
    # Convert both strings to lowercase for case-insensitive comparison
    bank_name = bank_name.lower()
    app_name = app_name.lower()

    # Calculate Jaccard Index
    jaccard_similarity = textdistance.jaccard(bank_name, app_name)

    return jaccard_similarity

def get_app_info(app_id):
    logging.info('start of get_app_info for ' + app_id)
    base_url = "https://gplayapi.cashlessconsumer.in/api/apps/"
    url = base_url + app_id
    logging.info(url)
    try:
        response = requests.get(url)

        if response.status_code == 200:
            data = response.json()
            return data
    except Exception as e:
         logging.info("An error occurred:", str(e))
         return []

def get_bank_app_info(bank_name):
    base_url = "https://gplayapi.cashlessconsumer.in/api/apps?q="
    url = base_url + bank_name

    try:
        response = requests.get(url)

        if response.status_code == 200:
            data = response.json()

            if data.get('results'):
                first_result = data['results'][0]
                app_name = first_result['title']
                app_id = first_result['appId']
                developer_name = first_result['developer']['devId']
                app_info = get_app_info(app_id)
                developer_website = app_info.get('developerWebsite', app_info.get('privacyPolicy'))

                return app_name, developer_name, developer_website, app_info['developerEmail'], app_info['maxInstalls']
            else:
                logging.info("No results found for", bank_name)
        else:
            return "Error:", response.status_code
    except Exception as e:
        return "An error occurred:", str(e)

def main():
    logging.basicConfig(filename='logs/BankAppTest' + time.strftime("%Y%m%d-%H%M%S") + '.log', format='%(asctime)s %(message)s', level=logging.INFO)
    # List of bank names to query
    bank_names = []
    with open('../data/MobileBankingApprovedBanks.csv', 'r') as csv_file:
        csv_reader = csv.reader(csv_file)
        for row in csv_reader:
            bank_names.extend(row)
    bank_info_list = []
    for bank_name in bank_names:
        app_name, developer_name, developer_website, developer_email, max_installs = get_bank_app_info(bank_name)
        bank_info = {}
        bank_info['app_name'] = app_name
        bank_info['developer_name'] = developer_name
        bank_info['developer_website'] = developer_website
        bank_info['developer_email'] = developer_email
        bank_info['max_installs'] = max_installs
        bank_info['bank_name'] = bank_name        
        bank_info['app_similarity'] = calculate_similarity(bank_name, bank_info['app_name'])
        bank_info['dev_similarity'] = calculate_similarity(bank_name, bank_info['developer_name'])

        if bank_info['app_similarity'] > 0.5 and bank_info['dev_similarity'] > 0.5:
            bank_info['category'] = 'A'
        elif bank_info['app_similarity'] > 0.5 or bank_info['dev_similarity'] > 0.5:
            bank_info['category'] = 'B'
        else:
            bank_info['category'] = 'C'

        bank_info_list.append(bank_info)

    with open('../data/Bank_App_Info.json', 'w') as file:
        json.dump(bank_info_list, file, indent=4)



if __name__ == "__main__":
    main()