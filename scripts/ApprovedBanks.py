import base64
import json
import logging
import os
import time
import requests
from bs4 import BeautifulSoup
from lxml import html

def main():
    # URL to the RBI page with the table
    url = 'https://www.rbi.org.in/Scripts/bs_viewcontent.aspx?Id=2463'

    # Send an HTTP GET request to the URL
    response = requests.get(url)

    # Check if the request was successful (status code 200)
    if response.status_code == 200:
        # Parse the HTML content with BeautifulSoup
        soup = BeautifulSoup(response.text, 'html.parser')

        # Find the table using BeautifulSoup's find method
        table = soup.find('table', attrs={"width":"85%"})
        print(table)

        # Check if a table was found
        if table:
            # Extract the data from the table
            rows = table.find_all('tr')
            for row in rows:
                columns = row.find_all('td')
                for column in columns:
                    print(column.get_text().strip())  # Print or process the data as needed
        else:
            print("No table found on the page.")
    else:
        print("Failed to retrieve the web page. Status code:", response.status_code)



if __name__ == "__main__":
    main()